import math
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
#функция вычисления математического ожидания
def expectedValue(x, y):
    nfeatures = x.shape[1]
    x = x.to_numpy()
    y = y.to_numpy()
    #создаём словарь, где ключом является метка целевого класса, а значением мат ожидание
    dictAvg = {a: [0 for elem in range(nfeatures)] for a in set(y)}
    #словарь счётчиков класса
    count = {a: 0 for a in set(y)}
    for i, xLine in enumerate(x):
        count[y[i]] += 1
        for j, elem in enumerate(xLine):
            dictAvg[y[i]][j] +=elem
    for key in list(count.keys()):
        for el in range(len(dictAvg[key])):
            dictAvg[key][el]/=count[key]
    return dictAvg, count

#функция вычисления дисперсии
def dispersion(x, y, expected, count):
    nfeatures = x.shape[1]
    x = x.to_numpy()
    y = y.to_numpy()
    dispersion = {a: [0 for elem in range(nfeatures)] for a in set(y)}
    for i, xLine in enumerate(x):
        for j, elem in enumerate(xLine):
            dispersion[y[i]][j] += (elem - expected[y[i]][j])**2
    for key in list(count.keys()):
        for el in range(len(dispersion[key])):
            dispersion[key][el]/=(count[key]-1)
    return dispersion

# загрузка данных
data = pd.read_csv("data/heartCl.csv")

# определение признаков и меток
features = [
    'Age', #возраст
    'Sex', #пол
    'ChestPainType', #тип боли в груди
    'RestingBP', #артериальное давление в состоянии покоя
    'Cholesterol', #холестерин сыворотки
    'FastingBS', #уровень сахара
    'RestingECG', #результаты экг покоя
    'MaxHR', #максимальный пульс
    'ExerciseAngina', #стенокардия при физ нагрузке
    'Oldpeak', #числовое значение степени депрессии сегмента
    'ST_Slope' #наклон пикового сегмента
]
X = data[features]
y = data['HeartDisease'].replace(0, -1) #заболевание сердца
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)
mean, count = expectedValue(X_train, y_train)
dis = dispersion(X_train, y_train, mean, count)
#априорные вероятности
priorProb = {a: count[a]/sum(count.values()) for a in set(y)}
#предсказание
ln1 = math.log(priorProb[1])
ln_1 = 5 * math.log(priorProb[-1])
y_pred = []
for i, xLine in enumerate(X_test.to_numpy()):
    a_1 = 0
    a1 = 0
    for j, elem in enumerate(xLine):
        a_1 -= ((elem - mean[-1][j])**2)/(2*(dis[-1][j]**2))
        a1 -= ((elem - mean[1][j])**2)/(2*(dis[1][j]**2))
    a_1 += ln_1
    a1 += ln1
    if a_1>a1:
        y_pred.append(-1)
    else:
        y_pred.append(1)
accuracy = np.mean(y_pred == y_test.values)
print('Точность:', accuracy*100)

# model = GaussianNB()
# model.fit(X_train, y_train)
# accuracy2 = np.mean(model.predict(X_test) == y_test.values)
# print('Точность:', accuracy2*100)