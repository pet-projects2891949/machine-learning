import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn import  preprocessing
from sklearn.model_selection import train_test_split, validation_curve, learning_curve
def date_to_days(dates):
    Days = []
    for date in dates: #16/12/2017 1/04/2017
        if (len(date) == 10):
            days = date[0:2]
            months = date[3:5]
            years = date[6:10]
            # для упрощения в месяце 30 дней, в году 365 дней
            totalDays = int(days) + int(months) * 30 + int(years) * 365
        else:
            days = date[0:1]
            months = date[2:4]
            years = date[5:9]
            # для упрощения: в месяце 30 дней, в году 365 дней
            totalDays = int(days) + int(months) * 30 + int(years) * 365
        Days.append(totalDays)
    return Days

#Вычисляет среднюю абсолютную процентную ошибку
def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

#Вычисляет медианную абсолютную процентную ошибку
def median_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.median(np.abs((y_true - y_pred) / y_true)) * 100

#Печатает рассчитанные значения коэффициента детерминации, средней и медианной абсолютных ошибок
def print_metrics(prediction, val_y):
    print('Средняя абсолютная ошибка: {:.3} %'.format(mean_absolute_percentage_error(val_y, prediction)))
    print('Медианная абсолютная ошибка: {:.3} %'.format(median_absolute_percentage_error(val_y, prediction)))

def print_info_dataset(df):
    print('Размер Dataframe:', df.shape)
    print('\n\nИнформация о Dataframe df.info():')
    print(df.info())


#Загрузка данных
df = pd.read_csv("data/housing_FULL.csv")
df = df.loc[0:999]
#Добавление столбца "дни"
df['Days'] = date_to_days(df['Date'])
df.dropna(subset=['Price'], inplace=True)
cat_columns = df.select_dtypes(['object']).columns
df[cat_columns] = df[cat_columns].apply ( lambda x: pd.factorize (x)[ 0 ])
y = df['Price']
#Создаем список признаков, на основании которых будем строить модели
features = [
            'Suburb',
            'Address',
            'Rooms',
            'Type',
            'Method',
            'SellerG',
            'Date',
            'Distance',
            'Postcode',
            #'Bedroom2',
            'Bathroom',
            #'Car',
            'Landsize',
            #'BuildingArea',
            #'YearBuilt',
            'CouncilArea',
            'Lattitude',
            'Longtitude',
            #'Regionname',
            'Propertycount',
            'Days'
           ]
for feature in features:
    df[feature].fillna(df[feature].median(), inplace=True)
scaler = preprocessing.MinMaxScaler()
X = scaler.fit_transform(df[features])
train_X, val_X, train_y, val_y = train_test_split (X, y,test_size=0.2, random_state=42)
train_y = train_y.reset_index()
del train_y['index']
param_range = [3, 5, 10, 30, 50, 100]
distances = pd.DataFrame()
pred = pd.DataFrame()
data_idx = []
val_accuracy = []
for n_neighbors in param_range:
    pred = pred[0:0]
    for test_dot in val_X:
        distances = distances[0:0]
        for i, train_dot in enumerate(train_X):
            dfNew = pd.DataFrame([{
                'train_idx': i,
                'dst': np.linalg.norm(train_dot - test_dot, ord=2)
            }])
            distances = pd.concat([dfNew, distances], ignore_index=True)
        nearest_neigbors_idxs = distances.sort_values('dst').head(n_neighbors).index
        for ind in nearest_neigbors_idxs:
            data_idx.append(train_y.iloc[ind])
        dfPred = pd.DataFrame([{'pred':np.mean(data_idx)}])
        pred = pd.concat([dfPred, pred], ignore_index=True)
        data_idx.clear()
    val_accuracy.append(mean_absolute_percentage_error(val_y, pred))
    print('k = ', n_neighbors)
    print_metrics(pred, val_y)
plt.plot(param_range, val_accuracy, label = 'Testing dataset accuracy')
plt.xlabel('n_neighbors')
plt.ylabel('Accuracy')
plt.show()