import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from sklearn import preprocessing
from sklearn.decomposition import PCA
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split

def visualize(X, y):
    pca = PCA(n_components=2)
    X = pca.fit_transform(X)

    x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
    y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
    h = 0.007
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    W = fit(X,y)
    x_t = np.c_[xx.ravel(), yy.ravel()]
    y_proba = sigmoid(np.dot(x_t, W))
    Z = np.where(y_proba >= 0.5, 1, 0)
    y_pred = sigmoid(np.dot(X, W))
    y_pred = np.where(y_pred >= 0.5, 1, 0)
    Z = Z.reshape(xx.shape)

    plt.figure(1, figsize=(12,12))
    plt.pcolormesh(xx,yy,Z,cmap=plt.cm.Paired)
    cor_pred = 0
    for i in range(len(y)):
        if y_pred[i] == y.values[i]:
            cor_pred += 1
        else:
            y_pred[i] = -100
    d1 = {0: 'lime', 1: 'm', -100: 'black'}
    plt.scatter(X[:, 0], X[:, 1], c=[d1[y_col] for y_col in y_pred], cmap=plt.cm.Paired)

    #plt.scatter(X[y == -1][:, 0], X[y == -1][:, 1], edgecolors='y')
    #plt.scatter(X[y == 1][:, 0], X[y == 1][:, 1], edgecolors='k')

    plt.xlim(xx.min(), xx.max())
    plt.ylim(yy.min(), yy.max())
    plt.xticks()
    plt.show()

def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def logloss(y, y_proba):
    y = y.to_numpy()
    logloss_1 = np.sum(np.log(y_proba[y == 1] + 1e-30))
    logloss_0 = np.sum(np.log(1 - y_proba[y == 0] + 1e-30))
    logloss_total = -(logloss_0 + logloss_1) / len(y)
    return logloss_total


def gr_logloss(X, W, y):
    y_proba = sigmoid(np.dot(X, W))
    grad = np.dot(X.transpose(),(y_proba - y))
    return grad

def fit (X_train, y_train):
    # установка минимального значения, на которое должны изменяться веса
    eps = 0.001
    # первоначальное точка
    W = np.random.randn(X_train.shape[1])
    # размер шага
    learning_rate = 0.001
    i = 0
    next_W = W

    while (True):
        cur_W = next_W
        # движение в негативную сторону вычисляемого градиента
        next_W = cur_W - learning_rate * gr_logloss(X_train, cur_W, y_train)
        # остановка когда достигнута необходимая степень точности
        if np.linalg.norm(cur_W - next_W) <= eps:
            return next_W
        i += 1


# загрузка данных
data = pd.read_csv("data/heartCl.csv")

# определение признаков и меток
features = [
    'Age', #возраст
    'Sex', #пол
    'ChestPainType', #тип боли в груди
    'RestingBP', #артериальное давление в состоянии покоя
    'Cholesterol', #холестерин сыворотки
    'FastingBS', #уровень сахара
    'RestingECG', #результаты экг покоя
    'MaxHR', #максимальный пульс
    'ExerciseAngina', #стенокардия при физ нагрузке
    'Oldpeak', #числовое значение степени депрессии сегмента
    'ST_Slope' #наклон пикового сегмента
]
scaler = preprocessing.MinMaxScaler()
X = scaler.fit_transform(data[features])
#X = np.c_[np.ones(len(X)), X]
y = data['HeartDisease']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
pca = PCA(n_components=2)
x_pca_train = pca.fit_transform(X_train)
next_W= fit(X_train, y_train)
y_proba = sigmoid(np.dot(X_train, next_W))
y_class = np.where(y_proba >= 0.5, 1, 0)
accuracy = np.mean(y_class == y_train)
print(f"Logloss в конце обучения: {logloss(y_train, y_proba)}")
print(f"Точность в конце обучения: {accuracy}")
#Предсказание
y_pred = sigmoid(np.dot(X_test, next_W))
y_pred = np.where(y_pred >= 0.5, 1, 0)
accuracy = np.mean(y_pred == y_test.values)
print(f"Logloss для тестовыйх значений: {logloss(y_test, y_pred)}")
print(f"Точность для тестовых значений: {accuracy}")
visualize(X_test, y_test)
# model = LogisticRegression()
# model.fit(X_train, y_train)
# model.predict(X_test)
# print(model.score(X_test, y_test))


