import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split, RandomizedSearchCV
from sklearn.tree import DecisionTreeClassifier, DecisionTreeRegressor, plot_tree
from sklearn. metrics import mean_absolute_error as mae
def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

def median_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.median(np.abs((y_true - y_pred) / y_true)) * 100

def mse(actual, pred):
    actual, pred = np.array(actual), np.array(pred)
    return np.square(np.subtract(actual,pred)).mean()

# Пример использования
data = pd.read_csv('data/result2022.csv', encoding='cp1251', sep=None)  # Загрузка данных
#data[['VUZ','Region','Type','Site']] = data[['VUZ','Region','Type','Site']].astype(float)
#print(data.dtypes())
X = data.copy()
del X['Rating']
y = data['Rating']
# Все корреляции с целевым параметром
correlations_data_Russiaedu = data.corr()['Rating'].sort_values()
correlations_data_interfax = data.corr()['Rating_Interfax'].sort_values()
correlations_data_res = (data.corr()['Rating'] - data.corr()['Rating_Interfax']).sort_values()

# correlations_data.hist(bins=100)
# plt.xlabel('Рост, см.')
# plt.ylabel('Частота')
# plt.show()
pd.set_option('display.max_rows', None)
print(f'\nКорреляции с целевым параметром по рейтингу Russiaedu: \n{correlations_data_Russiaedu}\n')
print(f'\nКорреляции с целевым параметром по рейтингу Интерфакс: \n{correlations_data_interfax}\n')
print(f'\nРазность корреляции между двумя рейтингами: \n{correlations_data_res}\n')
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=23)
df_model = DecisionTreeRegressor(min_samples_split = 5, max_features = 10, max_depth = 5)
df_model.fit(X_train, y_train)
pred = df_model.predict(X_test)
#print(y_test, pred)
MAPE = mean_absolute_percentage_error(y_test, pred)
MAE = mae(y_test, pred)
MedAPE = median_absolute_percentage_error(y_test, pred)
MSE = mse(y_test, pred)
print('Средняя абсолютная процентная ошибка: ',MAPE)
print('Средняя абсолютная ошибка: ',MAE)
print('Медианная абсолютная процентная ошибка: ',MedAPE)
fig = plt.figure(figsize=(50,40))
fig.canvas.toolbar.zoom()
_= plot_tree(df_model)
plt.show()
#print('Среднекватратическая ошибка: ',MSE)
# parametrs = {
#                'max_depth': [1, 3, 5,7,10,15,20,50,100],
#                 'min_samples_split': [2, 3, 5,7,10],
#                'max_features': ['auto', 'sqrt', 2,3,7,5,10,20,100],
#                 'random_state': [1, None],
#                 'criterion':['squared_error', 'friedman_mse', 'absolute_error', 'poisson']}
# model = DecisionTreeRegressor()
# random_src = RandomizedSearchCV(estimator = model,param_distributions = parametrs,
#                                cv = 20, n_iter = 10000, n_jobs=-1)
# random_src.fit(X_train, y_train)
# rf_prediction = random_src.predict(X_test).round(0)
# print(" Results from Random Search " )
# print("\n The best estimator across ALL searched params:\n", random_src.best_estimator_)
# print("\n The best score across ALL searched params:\n", random_src.best_score_)
# print("\n The best parameters across ALL searched params:\n", random_src.best_params_)

