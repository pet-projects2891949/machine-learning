import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
from sklearn import svm

def visualize_classification(X_train, y_train, X_test, y_test):
    pca = PCA(n_components=2)
    PCA_SVM = svm.SVC(kernel='linear', C=1)
    X_pca = pca.fit_transform(X_train)
    X_test_pca = pca.fit_transform(X_test)
    PCA_SVM.fit(X_pca, y_train)
    h = 0.1
    # создаём сетку для построения графика
    x_min, x_max = X_test_pca[:, 0].min() - 1, X_test_pca[:, 0].max() + 1
    y_min, y_max = X_test_pca[:, 1].min() - 1, X_test_pca[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))
    plt.subplot(1, 1, 1)
    Z = PCA_SVM.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    plt.contourf(xx, yy, Z, cmap=plt.cm.Paired, alpha=0.8)
    y_pred_pca = PCA_SVM.predict(X_test_pca)
    cor_pred = 0
    for i in range(len(y_pred_pca)):
        if y_pred_pca[i] == y_test.values[i]:
            cor_pred += 1
        else:
            y_pred_pca[i] = -100
    d1 = {-1: 'lime', 1: 'm', -100: 'black'}  # black = classification error
    plt.scatter(X_test_pca[:, 0], X_test_pca[:, 1], c=[d1[y] for y in y_pred_pca], cmap=plt.cm.Paired)
    plt.xlabel('x1')
    plt.ylabel('x2')
    plt.xlim(xx.min(), xx.max())
    plt.title('SVC')
    plt.show()

# определение SVM-классификатора
class CustomSVM:
    def __init__(self, etha= 0.00001, alpha= 0.01, n_iters=500):
        self.etha = etha
        self.alpha = alpha
        self.n_iters = n_iters
        self.w = None
        self.b = None

    def fit(self, X, y):
        n_samples, n_features = X.shape
        y = y.reset_index()
        del y['index']
        X = X.reset_index()
        del X['index']
        # инициализация весов и смещения
        self.w = np.zeros(n_features)
        self.b = 1
        X.index.name = None
        self.w = np.random.normal(loc=0.5, scale=0.1, size=n_features)
        # обучение SVM-классификатора
        for i in range(self.n_iters):
            for j, x in enumerate(X.iterrows()):
                x = x[1].to_numpy()
                y_val = y.iloc[j].values  # Получаем значение y для соответствующего индекса
                condition = y_val * (np.dot(x, self.w) - self.b)
                if condition >= 1:
                    self.w -= self.etha * self.alpha * self.w
                else:
                    self.w -= self.etha * (self.alpha * self.w - (x * y_val))
                    self.b -= self.etha * (self.alpha *self.b - y_val)
    def predict(self, X):
        y_pred = []
        X = X.reset_index()
        del X['index']
        for j, x in enumerate(X.iterrows()):
            x = x[1].to_numpy()
            y_pred.append(float(np.sign(np.dot(x, self.w) - self.b)))
        return y_pred

    def predictArr(self, X):
        y_pred = []
        for j,x in enumerate(X):
            y_pred.append(float(np.sign(np.dot(x, self.w) - self.b)))
        return y_pred

# загрузка данных
data = pd.read_csv("data/heartCl.csv")

# определение признаков и меток
features = [
    'Age', #возраст
    'Sex', #пол
    'ChestPainType', #тип боли в груди
    'RestingBP', #артериальное давление в состоянии покоя
    'Cholesterol', #холестерин сыворотки
    'FastingBS', #уровень сахара
    'RestingECG', #результаты экг покоя
    'MaxHR', #максимальный пульс
    'ExerciseAngina', #стенокардия при физ нагрузке
    'Oldpeak', #числовое значение степени депрессии сегмента
    'ST_Slope' #наклон пикового сегмента
]
X = data[features]
y = data['HeartDisease'].replace(0, -1) #заболевание сердца
# Все корреляции с целевым параметром
correlations_data = data.corr()['HeartDisease'].sort_values()
print(f'Корреляции с целевым параметром: \n{correlations_data}')
# разделение данных на обучающие и тестовые
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
# нормализация данных
#X_train = (X_train - np.mean(X_train, axis=0)) / np.std(X_train, axis=0)
#X_test = (X_test - np.mean(X_test, axis=0)) / np.std(X_test, axis=0)
# создание и обучение SVM-классификатора
SVM = CustomSVM()
SVM.fit(X_train, y_train)
# оценка точности модели на тестовых данных
y_pred = SVM.predict(X_test)
cor_pred = 0
for i in range(len(y_pred)):
    if y_pred[i] == y_test.values[i]:
        cor_pred += 1
    else:
        y_pred[i] = -100
accuracy = np.mean(y_pred == y_test.values)
print('Предсказано правильно', cor_pred, 'из', len(y_pred), 'Точность:', accuracy*100)
visualize_classification(X_train, y_train, X_test, y_test)

# pca = PCA(n_components=2)
#     PCA_SVM = svm.SVC(kernel='linear', C=1)
#     X_pca = pca.fit_transform(X_train)
#     X_test_pca = pca.fit_transform(X_test)
#     PCA_SVM.fit(X_pca, y_train)
#     y_pred_pca = PCA_SVM.predict(X_test_pca)
#     cor_pred = 0
#     for i in range(len(y_pred_pca)):
#         if y_pred_pca[i] == y_test.values[i]:
#             cor_pred += 1
#         else:
#             y_pred_pca[i] = -100
#     w_pca = PCA_SVM.coef_
#     print(w_pca)
#     d1 = {-1: 'lime', 1: 'm', -100: 'black'}  # black = classification error
#     plt.scatter(X_test_pca[:, 0], X_test_pca[:, 1], c=[d1[y] for y in y_pred_pca])
#     newline([0, -PCA_SVM.coef0 / w_pca[0][1]], [-PCA_SVM.coef0 / w_pca[0][0], 0], 'blue')
#     plt.show()